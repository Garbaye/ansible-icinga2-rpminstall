#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
cd ${ABSDIR}

type -P docker >/dev/null && DOCKER=$(which docker)
type -P podman >/dev/null && DOCKER=$(which podman)

ICINGA2_SOURCERPM='https://packages.icinga.com/fedora/39/release/src/icinga2/icinga2-2.14.5-1.fc39.src.rpm'
ICINGA_SELINUX_COMMON_SOURCERPM='https://packages.icinga.com/fedora/41/release/src/icinga-selinux-common/icinga-selinux-common-1.0.0-1.fc41.src.rpm'

ICINGA2_DIST=$(echo $ICINGA2_SOURCERPM | awk -F "/" '{print $NF}' | awk -F "." '{print $(NF-2)}')
ICINGA2_REVISION=$(echo $ICINGA2_SOURCERPM | awk -F "/" '{print $NF}' | awk -F ".${ICINGA2_DIST}" '{print $1}' | awk -F "-" '{print $NF}')
ICINGA_SELINUX_COMMON_DIST=$(echo $ICINGA_SELINUX_COMMON_SOURCERPM | awk -F "/" '{print $NF}' | awk -F "." '{print $(NF-2)}')
ICINGA_SELINUX_COMMON_REVISION=$(echo $ICINGA_SELINUX_COMMON_SOURCERPM | awk -F "/" '{print $NF}' | awk -F ".${ICINGA_SELINUX_COMMON_DIST}" '{print $1}' | awk -F "-" '{print $NF}')

mkdir /tmp/dockerrpmbuild-$$
for rocky_version in 8 9; do
    ${DOCKER} run -i --rm -v /tmp/dockerrpmbuild-$$:/out:Z docker.io/library/rockylinux:${rocky_version} <<EOF
rpm -E %{rhel} | grep -q 8 && {
  dnf -y install dnf-plugins-core hardlink mysql-devel
  dnf config-manager --set-enabled powertools
}
rpm -E %{rhel} | grep -q 9 && {
  dnf -y install --enablerepo=crb mysql-devel
}
dnf install -qy rpm-build rpmdevtools bison boost-devel cmake flex gcc-c++ libedit-devel libstdc++-devel logrotate make ncurses-devel openssl-devel postgresql-devel selinux-policy-devel systemd-devel
useradd -m dontbuildasroot -s /bin/bash
runuser -l dontbuildasroot -c 'rpm -i ${ICINGA2_SOURCERPM}'
runuser -l dontbuildasroot -c 'rpm -i ${ICINGA_SELINUX_COMMON_SOURCERPM}'
runuser -l dontbuildasroot -c 'sed -i "s|Release: ${ICINGA2_REVISION}\.${ICINGA2_DIST}$|Release: ${ICINGA2_REVISION}%{?dist}|" ~/rpmbuild/SPECS/icinga2.spec'
runuser -l dontbuildasroot -c 'sed -i "s|\.${ICINGA2_DIST}|\$(rpm -E %{dist})|g" ~/rpmbuild/SPECS/icinga2.spec'
# IF NOT x86_64
runuser -l dontbuildasroot -c 'uname -p | grep -q x86_64 || sed -i -e "s/x86_64/\$(uname -p)/g" ~/rpmbuild/SPECS/icinga2.spec'
# IF AARCH64
runuser -l dontbuildasroot -c 'uname -p | grep -q aarch64 && sed -i -e "s/-m64 //g" -e "s/-fcf-protection //g" ~/rpmbuild/SPECS/icinga2.spec'
runuser -l dontbuildasroot -c 'sed -i "s|Release: ${ICINGA_SELINUX_COMMON_REVISION}\.${ICINGA_SELINUX_COMMON_DIST}$|Release: ${ICINGA_SELINUX_COMMON_REVISION}%{?dist}|" ~/rpmbuild/SPECS/icinga-selinux-common.spec'
runuser -l dontbuildasroot -c 'spectool -g -R ~/rpmbuild/SPECS/icinga-selinux-common.spec'
runuser -l dontbuildasroot -c 'spectool -g -R ~/rpmbuild/SPECS/icinga2.spec'
runuser -l dontbuildasroot -c 'rpmbuild -ba ~/rpmbuild/SPECS/icinga-selinux-common.spec'
runuser -l dontbuildasroot -c 'rpmbuild -ba ~/rpmbuild/SPECS/icinga2.spec'
find ~dontbuildasroot/rpmbuild/ -name '*.rpm' -exec cp -f "{}" /out \;
EOF

    if [[ $? == 0 ]]; then
	mkdir -p ${ABSDIR}/files
	mv -f /tmp/dockerrpmbuild-$$/*.rpm ${ABSDIR}/files/
    else
	exit 1
    fi
done &&
rm -rf /tmp/dockerrpmbuild-$$
